package ma.activa.clientsmanagement.api;

import lombok.extern.slf4j.Slf4j;
import ma.activa.clientsmanagement.dto.ClientDto;
import ma.activa.clientsmanagement.dto.ClientPageDto;
import ma.activa.clientsmanagement.exception.NotFoundException;
import ma.activa.clientsmanagement.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/api")
public class ClientController {

    private static final Integer PAGESIZEPAGINATION = 5;

    @Autowired
    private ClientService clientService;

    @PostMapping(path = { "/client/save" }, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> saveNewClient(@RequestBody ClientDto clientDto){
        log.info("*******ClientController --> create Client with data : " + clientDto);

        ClientDto client = clientService.addNewClient(clientDto);

        return new  ResponseEntity<>(client, HttpStatus.OK);
    }

    @PutMapping(path = { "/client/update" }, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> updateClient(@RequestBody ClientDto clientDto) throws NotFoundException {
        log.info("*******ClientController --> update Client with data : " + clientDto);

        ClientDto client = clientService.updateClient(clientDto);

        return new  ResponseEntity<>(client, HttpStatus.OK);
    }

    @GetMapping(path = "/client/{code}")
    public ResponseEntity<ClientDto> getClient(
            @PathVariable("code") String code
    ) {
        log.info(" **** ClientController -- Get Client ");

        ClientDto clientDto = clientService.getClientByCode(code);

        return new ResponseEntity<>(clientDto, HttpStatus.OK);
    }

    @DeleteMapping(path = "/client/delete")
    public ResponseEntity<Boolean> deleteClient(
            @RequestParam("code") String code
    ) throws NotFoundException {
        log.info(" **** ClientController -- Get Client ");

        return new ResponseEntity<>(clientService.deleteClient(code), HttpStatus.OK);
    }

    @GetMapping(path = "/clients")
    public ResponseEntity<Page<ClientPageDto>> getClients(
            @RequestParam("page") String page,
            @RequestParam("sortField") String sortField,
            @RequestParam("sort") String sort,
            @RequestParam("pageSize") int pageSize) {

        pageSize = (ObjectUtils.isEmpty(pageSize)) ? PAGESIZEPAGINATION : pageSize;

        log.info(" **** ClientController -- Get Client pages");
        Page<ClientPageDto> pageDto = clientService.findClients(Integer.valueOf(page), pageSize,
                sortField, sort);

        return new ResponseEntity<>(pageDto, HttpStatus.OK);
    }

}
