package ma.activa.clientsmanagement.dto;

import lombok.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {

    private Long id;

    private String code;

    private String firstName;

    private String lastName;

    private String phone;

}
