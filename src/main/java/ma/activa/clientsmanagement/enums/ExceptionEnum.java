package ma.activa.clientsmanagement.enums;

public enum ExceptionEnum {

	CLIENT_NOT_FOUND("ERR_C_01", "Client not found");

	private String code;
	private String label;

	private ExceptionEnum(String code, String label) {
		this.code = code;
		this.label = label;
	}

	public String getCode() {
		return code;
	}

	public String getLabel() {
		return label;
	}

}
