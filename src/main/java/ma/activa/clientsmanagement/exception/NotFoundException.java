package ma.activa.clientsmanagement.exception;

import ma.activa.clientsmanagement.enums.ExceptionEnum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundException(ExceptionEnum exceptionEnum) {
		super(exceptionEnum);
	}

	public NotFoundException(ExceptionEnum exceptionEnum, String message) {
		super(exceptionEnum, message);
	}

	public NotFoundException(ExceptionEnum exceptionEnum, Object... args) {
		super(exceptionEnum, args);
	}
}
