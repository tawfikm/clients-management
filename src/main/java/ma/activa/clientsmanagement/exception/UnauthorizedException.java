package ma.activa.clientsmanagement.exception;

import ma.activa.clientsmanagement.enums.ExceptionEnum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnauthorizedException(ExceptionEnum exceptionEnum) {
		super(exceptionEnum);
	}

	public UnauthorizedException(ExceptionEnum exceptionEnum, String message) {
		super(exceptionEnum, message);
	}

	public UnauthorizedException(ExceptionEnum exceptionEnum, Object... args) {
		super(exceptionEnum, args);
	}

}
