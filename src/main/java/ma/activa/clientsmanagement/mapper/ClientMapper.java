package ma.activa.clientsmanagement.mapper;

import ma.activa.clientsmanagement.domain.Client;
import ma.activa.clientsmanagement.dto.ClientDto;
import ma.activa.clientsmanagement.dto.ClientPageDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientDto mapEntityIntoDto(Client client);

    @InheritInverseConfiguration
    Client mapDtoIntoEntity(ClientDto clientDto);

    default Page<ClientPageDto> mapEntityIntoPageDto(Page<Client> clientPage){
        return  clientPage.map(e-> ClientPageMapper.INSTANCE.mapEntityIntoPageDto(e));
    }
}
