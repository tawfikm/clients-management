package ma.activa.clientsmanagement.mapper;


import ma.activa.clientsmanagement.domain.Client;
import ma.activa.clientsmanagement.dto.ClientPageDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ClientPageMapper {

    ClientPageMapper INSTANCE = Mappers.getMapper(ClientPageMapper.class);

    ClientPageDto mapEntityIntoPageDto(Client client);
}
