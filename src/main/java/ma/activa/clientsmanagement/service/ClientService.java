package ma.activa.clientsmanagement.service;

import ma.activa.clientsmanagement.dto.ClientDto;
import ma.activa.clientsmanagement.dto.ClientPageDto;
import ma.activa.clientsmanagement.exception.NotFoundException;
import org.springframework.data.domain.Page;

public interface ClientService {

  ClientDto addNewClient(ClientDto client);

  ClientDto getClientByCode(String code);

  boolean deleteClient(String code) throws NotFoundException;

  ClientDto updateClient(ClientDto client) throws NotFoundException;

  Page<ClientPageDto> findClients(Integer page, int pageSize, String sortField, String sort);
}
