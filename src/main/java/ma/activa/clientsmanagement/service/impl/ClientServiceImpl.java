package ma.activa.clientsmanagement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.activa.clientsmanagement.domain.Client;
import ma.activa.clientsmanagement.dto.ClientDto;
import ma.activa.clientsmanagement.dto.ClientPageDto;
import ma.activa.clientsmanagement.enums.ExceptionEnum;
import ma.activa.clientsmanagement.exception.NotFoundException;
import ma.activa.clientsmanagement.mapper.ClientMapper;
import ma.activa.clientsmanagement.repository.ClientRepository;
import ma.activa.clientsmanagement.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientMapper clientMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ClientDto addNewClient(ClientDto client) {
        //TODO handle exception
        log.info("*******ClientService --> create Client with data : " + client);
        Client c = clientMapper.mapDtoIntoEntity(client);

        return clientMapper.mapEntityIntoDto(clientRepository.save(c));
    }

    @Transactional(readOnly = true)
    @Override
    public ClientDto getClientByCode(String code) {
        //TODO handle exception
        log.info("*******ClientService --> get Client with code : " + code);
        return clientMapper.mapEntityIntoDto(clientRepository.findByCode(code));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deleteClient(String code) throws NotFoundException {
        //TODO handle exception
        log.info("*******ClientService --> delete Client with code : " + code);
        Client client = clientRepository.findByCode(code);
        if (ObjectUtils.isEmpty(client)) {
            throw new NotFoundException(ExceptionEnum.CLIENT_NOT_FOUND);
        }

        clientRepository.delete(client);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ClientDto updateClient(ClientDto clientDto) throws NotFoundException {
        log.info("*******ClientService --> update Client with data : " + clientDto);
        Client client1 = clientRepository.findByCode(clientDto.getCode());

        if (ObjectUtils.isEmpty(client1))
            throw new NotFoundException(ExceptionEnum.CLIENT_NOT_FOUND);

        Client clientUpdate = clientMapper.mapDtoIntoEntity(clientDto);
        clientUpdate.setId(client1.getId());

        return clientMapper.mapEntityIntoDto(clientRepository.save(clientUpdate));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ClientPageDto> findClients(Integer page, int pageSize, String sortField, String sort) {

        //TODO handle exception
        log.info("*******ClientService --> get clients ");
        Sort sorting = sort.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
                : Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(page - 1, pageSize, sorting);
        Page<Client>  clientPage = clientRepository.findAll(pageable);

        return clientMapper.mapEntityIntoPageDto(clientPage);
    }
}
